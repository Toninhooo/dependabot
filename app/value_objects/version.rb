# frozen_string_literal: true

class Version
  # App version
  #
  # @return [String]
  def self.fetch
    @version ||= File.read("VERSION").strip # rubocop:disable Naming/MemoizedInstanceVariableName
  end
end
