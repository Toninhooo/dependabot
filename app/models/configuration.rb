# frozen_string_literal: true

# Configuration object
#
# @!attribute updates
#   @return [Array]
# @!attribute registries
#   @return [Registries]
class Configuration
  include Mongoid::Document

  field :forked, type: Boolean, default: false
  field :updates, type: Array, default: []
  field :registries, type: Registries, default: Registries.new({})

  embedded_in :project

  # Allowed registries for config entry
  #
  # @param [Hash] find_by
  # @return [Array<Hash>]
  def allowed_registries(**find_by)
    config_entry = entry(**find_by)
    return unless config_entry

    allowed_registries = config_entry[:registries]
    filter = allowed_registries == "*" ? ".*" : allowed_registries.join("|")

    registries.select(filter)
  end

  # Get single config entry
  #
  # @param [Hash] find_by
  # @return [Hash]
  def entry(**find_by)
    entries(**find_by).first
  end

  # Get config entries and symbolize keys
  #
  # @param [Hash] find_by
  # @return [Array<Hash>]
  def entries(**find_by)
    entries = updates.select { |conf| find_by.all? { |key, value| conf[key] == value } }

    updates_to_h(entries)
  end

  # Object comparator
  # @param [Configuration] other
  # @return [Booelan]
  def ==(other)
    self.class == other.class && updates_eql?(updates, other.updates) && registries == other.registries
  end

  private

  # Compare updates config
  #
  # @param [Array] updates
  # @param [Array] other_updates
  # @return [Boolean]
  def updates_eql?(updates, other_updates)
    updates_to_h(updates) == updates_to_h(other_updates)
  end

  # Convert array of BSON documents to array of hashes
  #
  # @param [Array<Object>] updates
  # @return [Array<Hash>]
  def updates_to_h(updates)
    return updates unless updates.any? { |entry| entry.is_a?(BSON::Document) }

    updates.map { |entry| entry.to_h.deep_symbolize_keys }
  end

  class Entity < Grape::Entity
    expose :forked,
           documentation: { type: "Boolean", desc: "Forked project" }
    expose :updates,
           documentation: { type: "Array", desc: "Array of configuration updates" },
           unless: ->(config, _options) { config.updates.empty? }
    expose :registries,
           using: Registries::Entity,
           unless: ->(config, _options) { config.registries.empty? }

    # Example response used by Swagger
    #
    # @return [Hash]
    def self.example_response
      {
        updates: [
          {
            package_manager: "docker",
            package_ecosystem: "docker",
            directory: "/docker",
            registries: "*",
            open_merge_requests_limit: 5,
            open_security_merge_requests_limit: 10,
            updater_options: {},
            reject_external_code: true,
            branch_name_prefix: "dependabot",
            branch_name_separator: "-",
            allow: [
              {
                dependency_type: "direct"
              }
            ],
            ignore: [],
            cron: "23 16 * * * UTC",
            rebase_strategy: {
              strategy: "auto"
            },
            vulnerability_alerts: {
              enabled: true,
              confidential: true
            }
          }
        ],
        registries: Registries::Entity.example_response,
        forked: false
      }
    end
  end
end
