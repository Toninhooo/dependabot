# frozen_string_literal: true

# :reek:InstanceVariableAssumption

class GrapeLogging < Grape::Middleware::Base
  include ApplicationHelper

  def before
    @start_time = Time.zone.now
  end

  def after
    attributes = {
      method: env["REQUEST_METHOD"],
      path: env["REQUEST_PATH"],
      format: env["api.format"],
      status: context.status,
      duration: (Time.zone.now - @start_time) * 1000
    }

    log(:info, ::Lograge.formatter.call(attributes))
  end
end
