# frozen_string_literal: true

module Job
  module Triggers
    class MergeRequestRecreate < ApplicationService
      def initialize(project_name, mr_iid, discussion_id = nil)
        @project_name = project_name
        @mr_iid = mr_iid
        @discussion_id = discussion_id
      end

      def call
        run_within_context({ job: "mr-update", project: project_name, mr: "!#{mr_iid}" }) do
          Dependabot::MergeRequest::RecreateService.call(
            project_name: project_name,
            mr_iid: mr_iid,
            discussion_id: discussion_id
          )
        end
      end

      private

      attr_reader :project_name, :mr_iid, :discussion_id
    end
  end
end
