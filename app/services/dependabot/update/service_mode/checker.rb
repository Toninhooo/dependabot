# frozen_string_literal: true

module Dependabot
  module Update
    module ServiceMode
      module Checker
        # List of advisories for dependency
        #
        # @return [Array<Vulnerability>]
        def vulnerabilities
          @vulnerabilities ||= Vulnerability.active_vulnerability(
            package_ecosystem: config_entry[:package_ecosystem],
            package: dependency.name
          )
        end
      end
    end
  end
end
