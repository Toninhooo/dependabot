# frozen_string_literal: true

# Prepend appropriate service mode methods to class
#
module ServiceModeConcern
  extend ActiveSupport::Concern

  included do |base|
    prepend Object.const_get(base.name.split("::").insert(-2, "ServiceMode").join("::")) if AppConfig.service_mode?
  end
end
