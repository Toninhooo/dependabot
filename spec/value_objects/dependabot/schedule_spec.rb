# frozen_string_literal: true

describe Dependabot::Schedule do
  subject(:cron) { described_class }

  let(:day) { "sunday" }
  let(:entry_a) { "a-bundler-/" }
  let(:entry_b) { "b-docker-/" }

  it "parses daily schedule configuration" do
    expect(cron.new(entry: entry_a, interval: "daily", day: day, time: "2:00").to_s).to eq("0 2 * * * UTC")
  end

  it "parses monthly schedule configuration" do
    expect(cron.new(entry: entry_a, interval: "monthly", day: day, time: "0:10").to_s).to eq("10 0 1 * * UTC")
  end

  it "generates randon cron hour in specific range" do
    expect(cron.new(entry: entry_a, interval: "daily", hours: "8-9").to_s).to match(/[0-59]+ [8-9] \* \* \* UTC/)
  end

  it "generates random cron hour based on project name" do
    cron_a = cron.new(entry: entry_a, interval: "daily", day: day).to_s
    cron_b = cron.new(entry: entry_b, interval: "daily", day: day).to_s

    cron_a_dup = cron.new(entry: entry_a, interval: "daily", day: day).to_s

    expect(cron_a).not_to eq(cron_b)
    expect(cron_a).to eq(cron_a_dup)
  end

  it "generates random cron day based on project name" do
    cron_a = cron.new(entry: entry_a, interval: "weekly", time: "2:00").to_s
    cron_b = cron.new(entry: entry_b, interval: "weekly", time: "2:00").to_s

    cron_a_dup = cron.new(entry: entry_a, interval: "weekly", time: "2:00").to_s

    expect(cron_a).not_to eq(cron_b)
    expect(cron_a).to eq(cron_a_dup)
  end
end
