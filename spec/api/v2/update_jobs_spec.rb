# frozen_string_literal: true

require_relative "../authenticated_endpoint"
require_relative "../paginated_endpoint"

describe V2::UpdateJobs, :integration, type: :request do
  include_context "with api helper"

  let(:project) { create(:project) }
  let(:update_job) { project.update_jobs.first }

  describe "/update_jobs", :aggregate_failures do
    let(:path) { "/api/v2/update_jobs" }

    context "basic auth integraton" do
      before do
        api_get(path)
      end

      it_behaves_like "basic auth endpoint"
    end

    context "get" do
      let(:update_job) do
        create(:update_job, project: project, package_ecosystem: Faker::Alphanumeric.alpha(number: 10))
      end

      it "returns all update jobs" do
        api_get(path, package_ecosystem: update_job.package_ecosystem)

        expect_status(200)
        expect_entity_response(Update::Job::Entity, [update_job])
      end

      context "with pagination" do
        before do
          create(
            :update_job,
            project: update_job.project,
            package_ecosystem: update_job.package_ecosystem,
            directory: "/#{Faker::Alphanumeric.alpha(number: 10)}"
          )

          api_get(path, package_ecosystem: update_job.package_ecosystem)
        end

        it_behaves_like "paginated endpoint", { total: 2, total_pages: 1 }
      end
    end
  end

  describe "/update_jobs/:id", :aggregate_failures do
    let(:path) { "/api/v2/update_jobs/#{update_job.id}" }

    context "get" do
      it "returns single update job" do
        api_get(path)

        expect_status(200)
        expect_entity_response(Update::Job::Entity, update_job)
      end
    end

    context "post" do
      before do
        allow(UpdateRunnerJob).to receive(:perform_later)
      end

      it "triggers updates" do
        api_post(path)

        expect_status(201)
        expect_json({ message: "Triggered dependency update job" })
        expect(UpdateRunnerJob).to have_received(:perform_later).with(
          project_name: project.name,
          package_ecosystem: update_job.package_ecosystem,
          directory: update_job.directory
        )
      end
    end
  end

  describe "/update_jobs/:id/runs", :aggregate_failures do
    let(:path) { "/api/v2/update_jobs/#{update_job.id}/runs" }

    let!(:failed_run) { create(:update_run_with_failures, job: update_job) }

    context "get" do
      before do
        create(:update_run, job: update_job)
      end

      it "returns update job runs" do
        api_get(path, has_failures: true)

        expect_status(200)
        expect_entity_response(Update::Run::Entity, [failed_run])
      end

      context "with pagination" do
        before do
          api_get(path)
        end

        it_behaves_like "paginated endpoint", { total: 2, total_pages: 1 }
      end
    end
  end

  describe "/update_jobs/:id/runs/:run_id", :aggregate_failures do
    let(:path) { "/api/v2/update_jobs/#{update_job.id}/runs/#{update_run.id}" }

    let!(:update_run) { create(:update_run, job: update_job) }

    context "get" do
      before do
        create(:update_run_with_failures, job: update_job)
      end

      it "returns update job run" do
        api_get(path)

        expect_status(200)
        expect_entity_response(Update::Run::Entity, update_run)
      end
    end
  end

  describe "/update_jobs/:id/runs/:run_id/log_entries", :aggregate_failures do
    let(:path) { "/api/v2/update_jobs/#{update_job.id}/runs/#{run.id}/log_entries" }
    let(:run) { create(:update_run, job: update_job) }

    before do
      create(:update_log_entry, run: run)
      create(:update_log_entry, run: run, level: 0)
    end

    context "get" do
      it "return update run log entries" do
        api_get(path, log_level: "INFO")

        expect_status(200)
        expect_entity_response(Update::LogEntry::Entity, [run.log_entries.first])
      end

      context "with pagination" do
        before do
          api_get(path, log_level: "DEBUG")
        end

        it_behaves_like "paginated endpoint", { total: 2, total_pages: 1 }
      end
    end
  end

  describe "/update_jobs/:id/runs/:run_id/failures", :aggregate_failures do
    let(:path) { "/api/v2/update_jobs/#{update_job.id}/runs/#{run.id}/failures" }
    let(:run) { create(:update_run_with_failures, job: update_job) }

    context "get" do
      it "return update run log entries" do
        api_get(path)

        expect_status(200)
        expect_entity_response(Update::Failure::Entity, run.failures)
      end

      context "with pagination" do
        before do
          run.save_errors!([{ message: "error", backtrace: "backtrace" }])

          api_get(path)
        end

        it_behaves_like "paginated endpoint", { total: 2, total_pages: 1 }
      end
    end
  end

  describe "/projects/:id/update_jobs", :aggregate_failures do
    let(:path) { "/api/v2/projects/#{project.id}/update_jobs" }

    context "get" do
      it "returns all update jobs for project" do
        api_get(path)

        expect_status(200)
        expect_entity_response(Update::Job::Entity, [update_job])
      end
    end
  end
end
