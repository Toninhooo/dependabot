import { test, expect } from "@playwright/test";
import { Smocker } from "@support/mocks/smocker";
import { mocks } from "@support/mocks/mocks";
import { user } from "@support/user";

import randomstring from "randomstring";

let smocker: Smocker;
let projectName: string;

const authHeader = user.basicAuthHeader;

test.beforeEach(async () => {
  projectName = randomstring.generate({ length: 10, charset: "alphabetic" });
  smocker = await new Smocker().init();

  await smocker.reset();
  await smocker.add(mocks.registerProject(projectName));
});

test.afterEach(async () => {
  await smocker.verify();
  await smocker.dispose();
});

test("adds new project without specific token", async ({ request }) => {
  const response = await request.post("/api/v2/projects", {
    headers: authHeader,
    data: { project_name: projectName }
  });
  const responseJson = await response.json();

  expect(response.ok()).toBeTruthy();
  expect(responseJson.name).toEqual(projectName);
});

test("adds new project with specific token", async ({ request }) => {
  const response = await request.post("/api/v2/projects", {
    headers: authHeader,
    data: { project_name: projectName, gitlab_access_token: "token" }
  });
  const responseJson = await response.json();

  expect(response.ok()).toBeTruthy();
  expect(responseJson.name).toEqual(projectName);
});
