# frozen_string_literal: true

describe Container::Kubernetes::Runner do
  let(:runner) do
    described_class.new(
      package_ecosystem: package_ecosystem,
      task_name: task_name,
      task_args: task_args
    )
  end

  let(:pod_name) { "runner-#{sha}-#{sha}" }
  let(:sha) { "123" }
  let(:project_name) { "project-name" }
  let(:package_ecosystem) { "bundler" }
  let(:directory) { "/directory" }
  let(:namespace) { "namespace" }
  let(:updater_template_path) { "/updater-template.yaml" }
  let(:result) { "Succeeded" }
  let(:task_name) { "update" }
  let(:task_args) { [project_name, package_ecosystem, directory] }

  let(:env) do
    {
      "SETTINGS__DEPLOY_MODE" => "k8s",
      "SETTINGS__UPDATER_TEMPLATE_PATH" => updater_template_path
    }
  end

  # rubocop:disable RSpec/VerifiedDoubles
  let(:client) do
    double(
      Container::Kubernetes::Client,
      create_pod: pod,
      get_pod_log: nil,
      delete_pod: nil
    )
  end

  let(:pod) { double("pod") }
  # rubocop:enable RSpec/VerifiedDoubles

  let(:pod_yml) do
    <<~YML
      ---
      metadata:
        name: runner-%<name_sha>s
        namespace: #{namespace}
      spec:
        containers:
          - image: image/%<package_ecosystem>s
            args:
              - "rake"
              - "%<rake_task>s"
    YML
  end

  let(:pod_hash) do
    {
      metadata: { name: pod_name, namespace: namespace },
      spec: {
        containers: [
          {
            image: "image/#{package_ecosystem}",
            args: ["rake", "dependabot:#{task_name}[#{project_name},#{package_ecosystem},#{directory}]"]
          }
        ]
      }
    }
  end

  before do
    allow(File).to receive(:read).and_call_original
    allow(File).to receive(:read).with(updater_template_path).and_return(pod_yml)
    allow(Container::Kubernetes::Client).to receive(:new).and_return(client)
    allow(SecureRandom).to receive(:alphanumeric).and_return(sha)

    allow(runner).to receive(:sleep)
    allow(pod).to receive_message_chain("status.phase").and_return("Pending", "Running", "Running", result) # rubocop:disable RSpec/MessageChain
    allow(client).to receive(:get_pod).with(pod_name, namespace).and_return(pod)
    allow($stdout).to receive(:puts)
  end

  around do |example|
    with_env(env) { example.run }
  end

  context "with successful update" do
    it "finishes update and removes container", :aggregate_failures do
      runner.call

      expect(client).to have_received(:create_pod) { |arg| expect(arg.to_h).to eq(pod_hash) }
      expect(client).to have_received(:get_pod_log).with(pod_name, namespace)
      expect(client).to have_received(:delete_pod)
    end
  end

  context "with unsuccessful update", :aggregate_failures do
    let(:result) { "Failed" }

    it "raises Container::Failure", :aggregate_failures do
      expect { runner.call }.to raise_error(Container::Failure, <<~ERR)
        Updater pod did not finish successfully.
        Expected pod phase to be 'Succeeded', got: '#{result}'
      ERR
      expect(client).to have_received(:delete_pod)
    end
  end

  context "with option delete_updater_container=false" do
    before do
      allow(UpdaterConfig).to receive(:delete_updater_container?).and_return(false)
    end

    it "skips pod deletion" do
      runner.call

      expect(client).not_to have_received(:delete_pod)
    end
  end
end
