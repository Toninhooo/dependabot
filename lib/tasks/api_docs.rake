# frozen_string_literal: true

require "grape-swagger/rake/oapi_tasks"

ENV["store"] = "tmp/api.json"

namespace :oapi do
  desc "clears generated documentation json …"
  task clear: :environment do
    FileUtils.rm_f("tmp/api_swagger_doc.json")
  end
end

GrapeSwagger::Rake::OapiTasks.new("V2::API")
Rake::Task["oapi:fetch"].prerequisites.unshift("oapi:clear")
